add_subdirectory(components)
add_subdirectory(client)
add_subdirectory(server)
add_subdirectory(socket-can-client)
# add_subdirectory(tester)
# add_subdirectory(test EXCLUDE_FROM_ALL)

# add_gcovr(${PROJECT_NAME} ${PROJECT_NAME}-test)