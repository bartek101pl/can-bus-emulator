/**
 * @file can_bus_client.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define clinet for the sdbus can emulator
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_BUS_CLIENT_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_BUS_CLIENT_H_

#include <sdbus-c++/sdbus-c++.h>

#include <mutex>
#include <queue>

#include "emulator/lib/can-bus-emulator/components/atomic_queue.h"
#include "emulator/lib/can-bus-emulator/components/can_frame.h"
#include "emulator/lib/can-bus-emulator/interface/can_bus_proxy.h"
namespace can_bus {
namespace client {
class CanBusClient
    : public sdbus::ProxyInterfaces<pl::simle::simba::canbus_proxy> {
 protected:
  components::AtomicQueue<components::CanFrame> frame_queue;
  uint32_t my_tranfer_id = 0;
  std::timed_mutex mutex;
  std::mutex send_lock;
  uint8_t interface_id = 0;
  bool send_falg = true;
  void (*callback)() = nullptr;

 private:
  void onReceiveAck(const uint32_t& transfer_id) override;
  void onNewFrame(const uint32_t& frame_id, const std::vector<uint8_t>& payload,
                  const uint32_t& transfer_id,
                  const uint8_t& interface_id) override;

 public:
  void SetInterfaceId(const uint8_t& interface_id);
  components::CanFrame GetFrame();
  bool FrameAvailable();
  bool TransferFrame(components::CanFrame frame);
  CanBusClient(const std::string& destination, const std::string& object_path);
  CanBusClient(const std::string& destination, const std::string& object_path,
               void (*callback)());
  ~CanBusClient();
};
}  // namespace client
}  // namespace can_bus

#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_BUS_CLIENT_H_
