/**
 * @file can_bus_client.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define clinet for the sdbus can emulator
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "emulator/lib/can-bus-emulator/client/can_bus_client.h"

#include <spdlog/spdlog.h>

#include <chrono>
#include <stdexcept>

namespace can_bus {
namespace client {

void CanBusClient::onReceiveAck(const uint32_t& transfer_id) {
  if (this->my_tranfer_id == transfer_id) {
    this->mutex.unlock();
  }
}
void CanBusClient::onNewFrame(const uint32_t& frame_id,
                              const std::vector<uint8_t>& payload,
                              const uint32_t& transfer_id,
                              const uint8_t& interface_id) {
  if (transfer_id != my_tranfer_id && interface_id == this->interface_id) {
    this->frame_queue.push(
        components::CanFrame{frame_id, payload, transfer_id, interface_id});
    try {
      this->SendAck(transfer_id);
    } catch (const sdbus::Error& err) {
    }
    if (this->callback) {
      this->callback();
    }
  }
  //   this->send_lock.unlock();
}
bool CanBusClient::TransferFrame(components::CanFrame frame) {
  try {
    this->my_tranfer_id =
        this->SendFrame(frame.GetId(), frame.GetPayload(), this->interface_id);
  } catch (const sdbus::Error& err) {
    return false;
  }
  this->mutex.try_lock();
  try {
    this->UnLock(this->my_tranfer_id);
  } catch (const sdbus::Error& err) {
    return false;
  }
  if (this->mutex.try_lock_for(std::chrono::seconds(1)) == true) {
    this->mutex.unlock();
    return true;
  }
  return false;
}
CanBusClient::CanBusClient(const std::string& destination,
                           const std::string& object_path)
    : ProxyInterfaces(destination, object_path) {
  this->registerProxy();
}
CanBusClient::CanBusClient(const std::string& destination,
                           const std::string& object_path, void (*callback)())
    : ProxyInterfaces(destination, object_path) {
  this->callback = callback;
  this->registerProxy();
}
CanBusClient::~CanBusClient() { this->unregisterProxy(); }

components::CanFrame CanBusClient::GetFrame() {
  if (this->frame_queue.empty()) {
    throw std::out_of_range("No frame to read");
  }
  components::CanFrame fr = this->frame_queue.front();
  this->frame_queue.pop();
  return fr;
}
bool CanBusClient::FrameAvailable() { return !this->frame_queue.empty(); }
void CanBusClient::SetInterfaceId(const uint8_t& interface_id) {
  this->interface_id = interface_id;
}
}  // namespace client
}  // namespace can_bus
