cmake_minimum_required(VERSION 3.1)
project(can-bus-client VERSION 1.0.1 DESCRIPTION "can bus client")
file(GLOB_RECURSE SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/*.cc")


find_package(sdbus-c++ REQUIRED)
find_package(spdlog CONFIG REQUIRED)

# find_package(nlohmann_json 3.2.0 REQUIRED)
add_library(${PROJECT_NAME} SHARED ${SOURCES})

# set_target_properties(${PROJECT_NAME} PROPERTIES VERSION ${PROJECT_VERSION})

target_link_libraries(${PROJECT_NAME}
    SDBusCpp::sdbus-c++
    spdlog::spdlog
    can-bus-components
    
)


# add_cppcheck(${PROJECT_NAME})
# add_cpplint(${PROJECT_NAME})

# install(TARGETS ${PROJECT_NAME} DESTINATION /usr/bin)
