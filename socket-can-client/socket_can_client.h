/**
 * @file socket_can_client.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define dbus client implementation to connect virtual can bus
 * which use dbus with linux socketcan
 * @version 0.1
 * @date 2023-02-24
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_SOCKET_CAN_CLIENT_SOCKET_CAN_CLIENT_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_SOCKET_CAN_CLIENT_SOCKET_CAN_CLIENT_H_
#include <linux/can.h>
#include <linux/can/raw.h>
#include <net/if.h>
#include <sdbus-c++/sdbus-c++.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <memory>
#include <thread>

#include "emulator/lib/can-bus-emulator/client/can_bus_client.h"
namespace can_bus {
namespace socket_can {
class SocketCanClient {
 private:
  client::CanBusClient dbus_client{
      pl::simle::simba::canbus_proxy::INTERFACE_NAME, "/pl/simle/simba/canbus"};
  int s;
  struct sockaddr_can addr;
  struct ifreq ifr;
  bool is_initialize = false;

  int8_t Init(const std::string& can_device);

 public:
  void Send();
  void operator()();
  SocketCanClient(/* args */);
  SocketCanClient(const std::string& can_device);
  ~SocketCanClient();
};

}  // namespace socket_can
}  // namespace can_bus

#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_SOCKET_CAN_CLIENT_SOCKET_CAN_CLIENT_H_