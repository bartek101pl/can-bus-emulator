/**
 * @file socket_can_client.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define dbus client implementation to connect virtual can bus
 * which use dbus with linux socketcan
 * @version 0.1
 * @date 2023-02-24
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "emulator/lib/can-bus-emulator/socket-can-client/socket_can_client.h"

#include <spdlog/spdlog.h>
#include <string.h>

namespace can_bus {
namespace socket_can {

void SocketCanClient::operator()() {
  if (!is_initialize) {
    return;
  }
  can_frame buffor;
  while (true) {
    if (recv(this->s, &buffor, sizeof(buffor), MSG_WAITALL) > 0) {
      this->dbus_client.TransferFrame(can_bus::components::CanFrame{
          buffor.can_id & 0x1FFFFFFF, buffor.data, buffor.can_dlc, 0,0});
    }
    std::this_thread::yield();
  }
}
SocketCanClient::SocketCanClient(const std::string& can_device) {
  if (this->Init(can_device) == 0) {
    this->is_initialize = true;
  } else {
    this->is_initialize = false;
  }
}
SocketCanClient::SocketCanClient(/* args */) {
  if (this->Init("vcan0") == 0) {
    this->is_initialize = true;
  } else {
    this->is_initialize = false;
  }
}
int8_t SocketCanClient::Init(const std::string& can_device) {
  if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
    spdlog::error("[SocketCanClient] An error occurred during creating socket");
    return -1;
  }

  strcpy(ifr.ifr_name, can_device.c_str());
  ioctl(this->s, SIOCGIFINDEX, &ifr);

  memset(&addr, 0, sizeof(addr));
  addr.can_family = AF_CAN;
  addr.can_ifindex = ifr.ifr_ifindex;

  if (bind(this->s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
    spdlog::error("[SocketCanClient] An error occurred during binding");
    return -2;
  }
  return 0;
}

void SocketCanClient::Send() {
  try {
    can_frame buffor;
    auto fr = this->dbus_client.GetFrame();
    buffor.can_id = fr.GetId() & 0x1FFFFFFF;
    if ((buffor.can_id & 0x1FFFF800) > 0) {
      buffor.can_id |= CAN_EFF_FLAG;
    }
    buffor.can_dlc = fr.GetPayload().size();
    for (size_t i = 0; i < buffor.can_dlc; i++) {
      buffor.data[i] = fr.GetPayload()[i];
    }
    if (write(s, &buffor, sizeof(buffor)) != sizeof(buffor)) {
      spdlog::error("[SocketCanClient] An error occurred during write frame");
    }
  } catch (const std::out_of_range& err) {
  }
}

SocketCanClient::~SocketCanClient() { close(this->s); }
}  // namespace socket_can
}  // namespace can_bus