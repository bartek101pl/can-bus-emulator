/**
 * @file can_bus_server.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define main can bus server
 * @version 0.1
 * @date 2023-02-17
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "emulator/lib/can-bus-emulator/server/can_bus_server.h"

#include <spdlog/spdlog.h>

#include <string>

namespace can_bus {
namespace server {
uint32_t CanBusServer::SendFrame(const uint32_t& frame_id,
                                 const std::vector<uint8_t>& payload,
                                 const uint8_t& interface_id) {
  if (this->buffor.find(std::to_string(last_transmission_id)) ==
      this->buffor.end()) {
    this->buffor.insert(
        {std::to_string(last_transmission_id),
         components::CanFrame{frame_id, payload, last_transmission_id,interface_id}});
    return last_transmission_id++;
  } else {
    return 0;
  }
}

void CanBusServer::UnLock(const uint32_t& transfer_id) {
  if (this->buffor.find(std::to_string(transfer_id)) != this->buffor.end()) {
    this->frame_to_send->push(this->buffor.at(std::to_string(transfer_id)));
    components::CanFrame fr = this->buffor.at(std::to_string(transfer_id));
    this->buffor.erase(std::to_string(transfer_id));
    if (this->socket_client != nullptr) {
      this->socket_client->Send();
    }
  }
}

void CanBusServer::SendAck(const uint32_t& transfer_id) {
  this->emitReceiveAck(transfer_id);
}

CanBusServer::CanBusServer(
    std::reference_wrapper<sdbus::IConnection> connection,
    const std::string& object_path, bool socket_can_enable)
    : AdaptorInterfaces(connection, object_path) {
  this->frame_to_send =
      std::make_shared<components::CommandQueue<components::CanFrame>>();
  this->read_mutex = std::make_shared<std::mutex>();
  this->send2_mutex = std::make_shared<std::mutex>();
  this->can_send_controller = std::make_unique<CanBusServerThread>(
      read_mutex, send2_mutex, frame_to_send, this);
  this->can_send_thread =
      std::make_unique<std::thread>(*this->can_send_controller);
  this->registerAdaptor();
  spdlog::info("Can server started");
  if (socket_can_enable) {
    this->socket_client =
        std::make_unique<can_bus::socket_can::SocketCanClient>();
    socket_can_thread = std::make_unique<std::thread>(
        [](can_bus::socket_can::SocketCanClient* client) { (*client)(); },
        socket_client.get());
  }
}
CanBusServer ::~CanBusServer() { this->unregisterAdaptor(); }
}  // namespace server
}  // namespace can_bus
