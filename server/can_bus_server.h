/**
 * @file can_bus_server.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define main can bus server
 * @version 0.1
 * @date 2023-02-17
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_H_

#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "emulator/lib/can-bus-emulator/components/can_frame.h"
#include "emulator/lib/can-bus-emulator/components/command_queue.h"
#include "emulator/lib/can-bus-emulator/interface/can_bus_adaptor.h"
#include "emulator/lib/can-bus-emulator/server/can_bus_server_thread.h"
#include "emulator/lib/can-bus-emulator/socket-can-client/socket_can_client.h"
namespace can_bus {
namespace server {
class CanBusServer
    : public sdbus::AdaptorInterfaces<pl::simle::simba::canbus_adaptor> {
 private:
  std::map<std::string, components::CanFrame> buffor;
  uint32_t last_transmission_id = 1;
  std::shared_ptr<std::mutex> read_mutex;
  std::shared_ptr<std::mutex> send2_mutex;
  std::shared_ptr<components::CommandQueue<components::CanFrame>>
      frame_to_send = nullptr;
  std::unique_ptr<CanBusServerThread> can_send_controller = nullptr;
  std::unique_ptr<std::thread> can_send_thread = nullptr;
  std::unique_ptr<can_bus::socket_can::SocketCanClient> socket_client = nullptr;
  std::unique_ptr<std::thread> socket_can_thread = nullptr;
  uint32_t SendFrame(const uint32_t& frame_id,
                     const std::vector<uint8_t>& payload,
                     const uint8_t& interface_id) override;
  void SendAck(const uint32_t& transfer_id) override;
  void UnLock(const uint32_t& transfer_id) override;

 public:
  CanBusServer(std::reference_wrapper<sdbus::IConnection> connection,
               const std::string& object_path, bool socket_can_enable);
  ~CanBusServer();
};
}  // namespace server
}  // namespace can_bus

#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_H_
