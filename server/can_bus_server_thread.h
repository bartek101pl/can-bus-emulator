/**
 * @file can_bus_server_thread.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define thread to make delay before send data at bus
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_THREAD_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_THREAD_H_

#include <memory>
#include <mutex>

#include "emulator/lib/can-bus-emulator/components/can_frame.h"
#include "emulator/lib/can-bus-emulator/components/command_queue.h"
#include "emulator/lib/can-bus-emulator/interface/can_bus_adaptor.h"
namespace can_bus {
namespace server {
class CanBusServerThread {
 private:
  std::shared_ptr<std::mutex> read_mutex;
  std::shared_ptr<std::mutex> send2_mutex;
  std::shared_ptr<components::CommandQueue<components::CanFrame>>
      frame_to_send = nullptr;

  sdbus::AdaptorInterfaces<pl::simle::simba::canbus_adaptor>* server = nullptr;

 public:
  void operator()();
  CanBusServerThread(
      std::shared_ptr<std::mutex> read_mutex,
      std::shared_ptr<std::mutex> send2_mutex,
      std::shared_ptr<components::CommandQueue<components::CanFrame>>
          frame_to_send,
      sdbus::AdaptorInterfaces<pl::simle::simba::canbus_adaptor>* server);
  ~CanBusServerThread();
};

}  // namespace server
}  // namespace can_bus
#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_SERVER_CAN_BUS_SERVER_THREAD_H_