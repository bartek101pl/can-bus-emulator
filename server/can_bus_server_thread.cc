/**
 * @file can_bus_server_thread.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define thread to make delay before send data at bus
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "emulator/lib/can-bus-emulator/server/can_bus_server_thread.h"

#include <mutex>
namespace can_bus {
namespace server {
void CanBusServerThread::operator()() {
  while (true) {
    if (!this->frame_to_send->empty()) {
      components::CanFrame fr = this->frame_to_send->front();
      this->frame_to_send->pop();
      this->server->emitNewFrame(fr.GetId(), fr.GetPayload(),
                                 fr.GetTransferId(), fr.GetInterfaceId());
    } else {
      this->frame_to_send->WaitForData();
    }
  }
}
CanBusServerThread::CanBusServerThread(
    std::shared_ptr<std::mutex> read_mutex,
    std::shared_ptr<std::mutex> send2_mutex,
    std::shared_ptr<components::CommandQueue<components::CanFrame>>
        frame_to_send,
    sdbus::AdaptorInterfaces<pl::simle::simba::canbus_adaptor>* server) {
  this->frame_to_send = frame_to_send;
  this->read_mutex = read_mutex;
  this->send2_mutex = send2_mutex;
  this->server = server;
}

CanBusServerThread::~CanBusServerThread() {}
}  // namespace server
}  // namespace can_bus