/**
 * @file can_frame.cc
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define class to hold frame data
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#include "emulator/lib/can-bus-emulator/components/can_frame.h"

#include <algorithm>

namespace can_bus {
namespace components {
CanFrame::CanFrame(const uint32_t& id, const std::vector<uint8_t>& payload,
                   const uint32_t& transfer_id, const uint8_t& interface_id) {
  this->transfer_id = transfer_id;
  this->id = id;
  this->interface_id = interface_id;
  std::copy(payload.begin(), payload.end(), std::back_inserter(this->payload));
}
CanFrame::CanFrame(const uint32_t& id, uint8_t payload[], const uint8_t& size,
                   const uint32_t& transfer_id, const uint8_t& interface_id) {
  this->transfer_id = transfer_id;
  this->id = id;
  this->interface_id = interface_id;
  this->payload.insert(this->payload.begin(), payload, payload + size);
}
uint32_t CanFrame::GetId() { return this->id; }
uint32_t CanFrame::GetTransferId() { return this->transfer_id; }
std::vector<uint8_t> CanFrame::GetPayload() { return this->payload; }
uint8_t CanFrame::GetInterfaceId() { return this->interface_id; }
}  // namespace components
}  // namespace can_bus