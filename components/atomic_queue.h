/**
 * @file atomic_queue.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define atomic queue it's mean this queue have also mutex to
 * protect for multi thread application
 * @version 1.0
 * @date 2023-02-10
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_ATOMIC_QUEUE_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_ATOMIC_QUEUE_H_

#include <mutex>  // NOLINT
#include <queue>  // NOLINT

namespace can_bus{
namespace components {
template <typename T>
class AtomicQueue : public std::queue<T> {
 protected:
  std::mutex queue_mutex;

 public:
  inline bool empty() {
    this->queue_mutex.lock();
    bool res = std::queue<T>::empty();
    this->queue_mutex.unlock();
    return res;
  }

  inline T front() {
    this->queue_mutex.lock();
    T res = std::queue<T>::front();
    this->queue_mutex.unlock();
    return res;
  }

  inline void pop() {
    this->queue_mutex.lock();
    std::queue<T>::pop();
    this->queue_mutex.unlock();
  }

  inline void push(const T& data) {
    this->queue_mutex.lock();
    std::queue<T>::push(data);
    this->queue_mutex.unlock();
  }
};
}  // namespace components
}  // namespace can_bus

#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_ATOMIC_QUEUE_H_
