/**
 * @file can_frame.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define class to hold frame data
 * @version 0.1
 * @date 2023-02-18
 *
 * @copyright Copyright (c) 2023
 *
 */
#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_FRAME_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_FRAME_H_

#include <stdint.h>

#include <vector>
namespace can_bus {
namespace components {
class CanFrame {
 protected:
  std::vector<uint8_t> payload;
  uint32_t id = 0;
  uint32_t transfer_id = 0;
  uint8_t interface_id = 0;
  /* data */
 public:
  CanFrame(const uint32_t& id, const std::vector<uint8_t>& payload,
           const uint32_t& transfer_id, const uint8_t& interface_id);
  CanFrame(const uint32_t& id, uint8_t payload[], const uint8_t& size,
           const uint32_t& transfer_id, const uint8_t& interface_id);
  uint32_t GetId();
  uint32_t GetTransferId();
  std::vector<uint8_t> GetPayload();
  uint8_t GetInterfaceId();
  virtual ~CanFrame() = default;
};
}  // namespace components
}  // namespace can_bus
#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_CLIENT_CAN_FRAME_H_