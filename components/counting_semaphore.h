/**
 * @file counting semaphore.h
 * @author  Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define counting semaphore
 * @version 1.0
 * @date 2023-02-09
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_COUNTING_SEMAPHORE_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_COUNTING_SEMAPHORE_H_

#include <mutex>  // NOLINT

namespace can_bus {
namespace components {
class CountingSemaphore {
 private:
  /// @brief this mutex blocking the release function if count is equals max
  std::mutex up;
  /// @brief this mutex blocking the take function if count is equals min
  std::mutex down;
  /// @brief Number of release functions minus number of Take functions
  uint8_t count = 0;
  /// @brief maximum value of the count
  uint8_t max = 0;

 public:
  /// @brief Initialize counting semaphore with setted max value and init count
  /// value
  /// @param init_count initial value of the cout
  /// @param max_count max value of the cout
  explicit CountingSemaphore(const uint8_t& init_count, const uint8_t& max_count);
  /// @brief Initialize counting semaphore with only init count value
  /// @param init_count initial value of the cout
  explicit CountingSemaphore(const uint8_t& init_count);
  /// @brief This function adds one for each call, and if the number equals the
  /// maximum it blocking
  void Release();
  /// @brief This function subtracts one for each call, and if the number is
  /// equal to 0, it blocks
  void Take();

  void TakeWithoutSub();
  ~CountingSemaphore();
};

}  // namespace components
}  // namespace can_bus
#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_COUNTING_SEMAPHORE_H_
