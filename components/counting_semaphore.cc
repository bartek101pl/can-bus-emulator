/**
 * @file counting semaphore.h
 * @author  Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define counting semaphore
 * @version 1.0
 * @date 2023-02-09
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "emulator/lib/can-bus-emulator/components/counting_semaphore.h"

namespace can_bus{
namespace components {
CountingSemaphore::CountingSemaphore(const uint8_t& init_count,
                                     const uint8_t& max_count) {
  this->count = init_count;
  this->max = max_count;
}
CountingSemaphore::CountingSemaphore(const uint8_t& init_count) {
  this->count = init_count;
  this->max = 0u;
}
void CountingSemaphore::Release() {
  if (this->max != 0) {
    if (count < this->max) {
      this->count++;
      down.unlock();
    } else {
      up.lock();
      up.lock();
      up.unlock();
      this->count++;
    }
  } else {
    this->count++;
    down.unlock();
  }
}
void CountingSemaphore::Take() {
  if (count > 0) {
    count--;
    up.unlock();
  } else {
    down.try_lock();
    down.lock();
    down.unlock();
    count--;
  }
}
void CountingSemaphore::TakeWithoutSub() {
  if (count==0){
    down.try_lock();
    down.lock();
    down.unlock();
  }
}
CountingSemaphore::~CountingSemaphore() {
  up.unlock();
  down.unlock();
}
}  // namespace components
}  // namespace can_bus
