/**
 * @file command_queue.h
 * @author Bartosz Snieg (snieg45@gmail.com)
 * @brief This file define command queue it's mean this queue have also mutex to
 * protect for multi thread application and integrated counting semapthore
 * @version 1.0
 * @date 2023-02-16
 *
 * @copyright Copyright (c) 2023
 *
 */

#ifndef EMULATOR_LIB_CAN_BUS_EMULATOR_QUEUE_COMMAND_QUEUE_H_
#define EMULATOR_LIB_CAN_BUS_EMULATOR_QUEUE_COMMAND_QUEUE_H_
#include "emulator/lib/can-bus-emulator/components/atomic_queue.h"
#include "emulator/lib/can-bus-emulator/components/counting_semaphore.h"
namespace can_bus {
namespace components {
template <typename T>
class CommandQueue : public AtomicQueue<T> {
 private:
  CountingSemaphore sem{0};

 public:
  inline void pop() {
    AtomicQueue<T>::pop();
    sem.Take();
  }
  inline void WaitForData() {
    this->sem.TakeWithoutSub();
  }
  inline void push(const T& data) {
    AtomicQueue<T>::push(data);
    sem.Release();
  }
};
}  // namespace components
}  // namespace can_bus
#endif  // EMULATOR_LIB_CAN_BUS_EMULATOR_COMMAND_QUEUE_H_
